<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"><!--mobile devices -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='//fonts.googleapis.com/css?family=Alef' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Assistant' rel='stylesheet'>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="styles/home.css"/>
        <link rel="stylesheet" type="text/css" href="styles/bootstrap-social.css.css"/>
        <link rel="stylesheet" href="http://path/to/font-awesome/css/font-awesome.min.css">
        <script src="https://use.fontawesome.com/6092883b9c.js"></script>
        <title>Live Issue Tracking </title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center block-center login_page">
                    <h1>Live Issue Monitoring Application</h1><br><br>
                    
                        <fieldset>
                            <legend>Enter your login credentials here</legend> 
                            <form method="post" action="Servlet_login_control">                                                       
                                <p>${message}</p>
                                <input type="text" name="user_name" placeholder="Enter your mail id" size="35" class="text-center"><br>
                                <input type="password" name="password" placeholder="Enter your password" size="35" class="text-center"><br>
                                <input type="submit" name="login" value="Login" class="customize_btn btn btn-primary  btn-transparent btn-lg" ><br>
                            </form>
                        </fieldset>
                    
                </div>
            </div>
        </div>
    </body>
</html>
