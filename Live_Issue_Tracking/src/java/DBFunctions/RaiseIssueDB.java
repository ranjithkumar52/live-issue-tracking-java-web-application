/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBFunctions;

import Javabeans.Issue_Tracker_Data;
import Util.ConnectionPool;
import Util.DBUtil;
import java.sql.*;
import java.util.ArrayList;

public class RaiseIssueDB {

    public static int insert(Issue_Tracker_Data issue_tracker_data) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query
                = "INSERT INTO issue_tracker (title, table_number, student_name, issue_description)"
                + "VALUES (?, ?, ?, ?)";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, issue_tracker_data.gettitle());
            ps.setString(2, issue_tracker_data.gettable_number());
            ps.setString(3, issue_tracker_data.getstudent_name());
            ps.setString(4, issue_tracker_data.getissue_description());
            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static int update(Issue_Tracker_Data signup_data) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "UPDATE SignUpData SET "
                + "Name = ?, "
                + "WHERE Email = ?";
        try {
            ps = connection.prepareStatement(query);
            //ps.setString(1, signup_data.getname());
            //ps.setString(2, signup_data.getemail());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static int delete(Issue_Tracker_Data issue_tracker_data) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        

        String query = "DELETE FROM issue_tracker "
                + "WHERE student_name = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, issue_tracker_data.getstudent_name());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static boolean studentExists(String student_name) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT student_name FROM issue_tracker "
                + "WHERE student_name = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, student_name);
            rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }

    public static Issue_Tracker_Data selectUser(String student_name) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        //System.out.println("Inside select user: The value of student_name is "+student_name);
        String query = "SELECT * FROM issue_tracker "
                + "WHERE student_name = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, student_name);
            
            rs = ps.executeQuery();
            Issue_Tracker_Data issue_tracker_data = null;
            if (rs.next()) {
                issue_tracker_data = new Issue_Tracker_Data();
                issue_tracker_data.settitle(rs.getString("title"));
                issue_tracker_data.settable_number(rs.getString("table_number"));
                issue_tracker_data.setstudent_name(rs.getString("student_name"));
                issue_tracker_data.setissue_description(rs.getString("issue_description")); 
            }
            return issue_tracker_data;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    } 
    
    public static ArrayList<Issue_Tracker_Data> displayAll() {
        // add code that returns an ArrayList<User> object of all users in the User table
        ArrayList<Issue_Tracker_Data> userList= new ArrayList<Issue_Tracker_Data>();
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM issue_tracker ";
        try {
            ps = connection.prepareStatement(query);
            //ps.setString(1, email);
            rs = ps.executeQuery();
            //System.out.println("the value of rs "+rs);
            Issue_Tracker_Data issue_tracker_data = null;
            while (rs.next()) {
                issue_tracker_data = new Issue_Tracker_Data();
                issue_tracker_data.settitle(rs.getString("title"));
                issue_tracker_data.settable_number(rs.getString("table_number"));
                issue_tracker_data.setstudent_name(rs.getString("student_name"));
                issue_tracker_data.setissue_description(rs.getString("issue_description"));
                userList.add(issue_tracker_data);
            }
            return userList;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
       
    }

    
}
