package Javabeans;
import java.io.Serializable;

public class Issue_Tracker_Data implements Serializable{
    private String title;
    private String table_number;
    private String student_name;
    private String issue_description;

    
    public Issue_Tracker_Data(){
        title = "null";
        table_number = "null";
        student_name = "null";
        issue_description = "null";

    }
    
    public Issue_Tracker_Data(String title, String table_number, String student_name, String issue_description){
        this.title = title;
        this.table_number = table_number;
        this.student_name = student_name;
        this.issue_description = issue_description;
    }
    
    public String gettitle(){
        return title;
    }
    
    public String gettable_number(){
        return table_number;
    }
    
    public String getstudent_name(){
        return student_name;
    }
    
    public String getissue_description(){
        return issue_description;
    }
    
   
    public void settitle(String title){
        this.title = title;
    }
    
    public void settable_number(String table_number){
        this.table_number =table_number;
    }
    
    public void setstudent_name(String student_name){
        this.student_name = student_name;
    }
    
    public void setissue_description(String issue_description){
        this.issue_description = issue_description;
    }
    
}

