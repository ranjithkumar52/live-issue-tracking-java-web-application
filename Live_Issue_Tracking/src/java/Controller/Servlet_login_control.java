/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Servlet_login_control extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
            String url = "/index.jsp";
            String user_name = request.getParameter("user_name");
            String password = request.getParameter("password");
            System.out.println("user_name "+user_name+"password "+password);
            String message = "login failed. Please enter valid credentials";
            //instructor credentials
            if("admin".equals(user_name) && "password".equals(password)){
                url = "/Servlet_issue_tracking_TA";
                message="welcome Instructor";
            }
            //student1 credentials
            if("student1".equals(user_name) && "password".equals(password) || 
                    ("student2".equals(user_name) && "password".equals(password)) || 
                    ("student3".equals(user_name) && "password".equals(password))){
                 url="/request_form.jsp";
                 message = "login successful";
            }
            
            request.setAttribute("message", message);
            getServletContext().getRequestDispatcher(url).forward(request, response);
            
        
    }
        
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
