/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DBFunctions.RaiseIssueDB;
import Javabeans.Issue_Tracker_Data;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Servlet_issue_tracking_TA extends HttpServlet {
    
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

            HttpSession session = request.getSession();
            String url="/issue_tracking_table_Instructors.jsp";            
            
            //delete function
            String student_name = request.getParameter("student_name");
            if(student_name != null){
                Issue_Tracker_Data issue_tracker_data = new Issue_Tracker_Data();
                issue_tracker_data = RaiseIssueDB.selectUser(student_name);
                RaiseIssueDB.delete(issue_tracker_data);
            }
            
            //displayAll
            ArrayList<Issue_Tracker_Data> issue_tracker_data1 = RaiseIssueDB.displayAll();
            
            //request.setAttribute("issue_tracker_data1", issue_tracker_data1);
            session.setAttribute("issue_tracker_data1", issue_tracker_data1);
            getServletContext().getRequestDispatcher(url).forward(request, response);
            
            
            
        
    }
        
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
