package Controller;

import DBFunctions.RaiseIssueDB;
import Javabeans.Issue_Tracker_Data;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Servlet_request_form_control extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
        String url="/request_form.jsp";
        String title = request.getParameter("title");
        String table_number = request.getParameter("table_number");
        String student_name = request.getParameter("student_name");
        String issue_description = request.getParameter("issue_description");
        String message = "Please fill out all the details";
        
        
        //change the below code later for input validation
        if(title != null && table_number != null && student_name != null && issue_description != null) {
          Issue_Tracker_Data issue_tracker_data = new Issue_Tracker_Data(title, table_number, student_name, issue_description);
          RaiseIssueDB.insert(issue_tracker_data);
          url="/issue_tracking_table_students.jsp";
        }
        

        ArrayList<Issue_Tracker_Data> issue_tracker_data1 = RaiseIssueDB.displayAll();
        request.setAttribute("issue_tracker_data1", issue_tracker_data1);
        getServletContext().getRequestDispatcher(url).forward(request, response);
            
        
    }
        
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
