<%-- 
    Document   : issue_tracking_table
    Created on : Apr 2, 2017, 9:16:03 PM
    Author     : Ranjith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"><!--mobile devices -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='//fonts.googleapis.com/css?family=Alef' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Assistant' rel='stylesheet'>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="styles/home.css"/>
        <link rel="stylesheet" type="text/css" href="styles/bootstrap-social.css.css"/>
        <link rel="stylesheet" href="http://path/to/font-awesome/css/font-awesome.min.css">
        <script src="https://use.fontawesome.com/6092883b9c.js"></script>
        <title>Live Issue Tracking </title>
    </head>
    <body>
        <div class="container">
            <div class='row'>
                <div class='col-md-12 text-center block-center'>
                    <h1 class="text-center block-center">Issue Tracking Table (student's page)</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center block-center login_page">
                    <a href="index.jsp">Home page</a>
                        <table class="table text-center block-center">
                        <thead>
                          <tr>
                            <th class="text-center block-center">Title of the request</th>
                            <th class="text-center block-center">Table Number</th>
                            <th class="text-center block-center">Student Name</th>
                            <th class="text-center block-center">Issue Description</th>
                          </tr>
                        </thead>
                        
                        <tbody>                        
                        <c:forEach var="issue_tracker_data1" items="${issue_tracker_data1}">
                            <tr>
                        
                                <td class="text-center block-center">${issue_tracker_data1.title}</td>
                                <td class="text-center block-center">${issue_tracker_data1.table_number}</td>
                                <td class="text-center block-center">${issue_tracker_data1.student_name}</td>
                                <td class="text-center block-center">${issue_tracker_data1.issue_description}</td>
                            </tr> 
                        </c:forEach>
                        
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
         
    </body>
</html>
