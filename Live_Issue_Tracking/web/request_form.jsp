
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"><!--mobile devices -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='//fonts.googleapis.com/css?family=Alef' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Assistant' rel='stylesheet'>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="styles/home.css"/>
        <link rel="stylesheet" type="text/css" href="styles/bootstrap-social.css.css"/>
        <link rel="stylesheet" href="http://path/to/font-awesome/css/font-awesome.min.css">
        <script src="https://use.fontawesome.com/6092883b9c.js"></script>
        <title>Live Issue Tracking </title>
    </head>
    <!-- later today, use session attribute to keep track of the customers -->
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center block-center login_page">
                    <form method="post" action="Servlet_request_form_control">
                        <h3 class="text-center block-center">Fill the details to request an issue</h3>
                        <p>${message}</p>
                        <input type="text" name="title" placeholder="Title of the request" required class="text-center" size="35"><br><br>
                        <input type="text" name="table_number" placeholder="Table Number" required class="text-center" size="35"><br><br>
                        <input type="text" name="student_name" placeholder="Student Name" required class="text-center" size="35"><br><br>
                        <textarea name="issue_description" rows="5" cols="35" required class='text-center center-block'
                                              placeholder="Issue Description" maxlength="35"  id="issue_description_text_area"></textarea>
                        <input type="submit" name="login" value="Raise Issue" class="customize_btn btn btn-primary  btn-transparent btn-lg" ><br>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
